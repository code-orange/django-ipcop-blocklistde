from __future__ import absolute_import, unicode_literals

from itertools import chain

import requests
from celery import shared_task
from django.conf import settings
from requests import ConnectionError, ReadTimeout

from django_ipcop_models.django_ipcop_models.models import *


@shared_task(name="ipcop_blocklistde_sync_abuses")
def ipcop_blocklistde_sync_abuses():
    api_url = "https://www.blocklist.de/de/httpreports.html"

    abuses_no_sync_ip4 = IpcopAbuseIp4.objects.exclude(sync_partners_out=2).order_by(
        "id"
    )
    abuses_no_sync_ip6 = IpcopAbuseIp6.objects.exclude(sync_partners_out=2).order_by(
        "id"
    )
    abuses_no_sync = list(chain(abuses_no_sync_ip4, abuses_no_sync_ip6))

    if len(abuses_no_sync) < 1:
        return

    for abuse in abuses_no_sync:
        all_cats = list()

        for category in abuse.categories.all():
            all_cats.append(str(category.id))

        category_txt = ",".join(all_cats)

        try:
            r = requests.post(
                api_url,
                data={
                    "server": settings.IPCOP_BLOCKLISTDE_API_ID,
                    "apikey": settings.IPCOP_BLOCKLISTDE_API_KEY,
                    "ip": str(abuse.ip.ipaddr),
                    "service": "bruteforcelogin",
                    "format": "json",
                    "logs": abuse.comment,
                },
                timeout=20,
            )
        except (ConnectionError, ReadTimeout):
            # Ignore connection errors and wait for next run
            return

        if r.status_code == 200 or r.status_code == 201:
            abuse.sync_partners_out.add("2")
            abuse.save()

    return
